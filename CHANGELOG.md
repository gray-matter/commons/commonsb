# Notable Changes

All notable changes will be logged here (as of version 4.0.2-alpha). For The project adheres to semantic versioning.

## 4.0.4-alpha

- ***Added*** a `tr` function trim raw text to a single line. Good for building configuration/url strings, but use raw strings to aid in readablity.
- ***Updated*** documentation on the `Either` class.
- ***Added*** `swap` function on `Either` class to swap the error with the result, or visa versa.  

## 4.0.2-alpha 

- ***Changed*** the `Result` monad as the naming conflicts with Kotlin's built-in class.  The class is now called `Either`, and conforms to then general use of this monad. 
- ***Dropped*** the convenience extension `val Either.value`, and replaced it with the `getOrThrow()` family of functions.   
- ***Added*** functions to process `Either` instances in a pipeline to make it more readable, such as `let()` to start a call flow. 
- ***Added*** a `props()` function to instantiate  an  instance of a `java.util.Properties` class. Seemed a bit more idiomatic use of Kotlin. 😏

