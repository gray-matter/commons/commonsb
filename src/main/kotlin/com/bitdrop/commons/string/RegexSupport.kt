package com.bitdrop.commons.string

import org.intellij.lang.annotations.Language

/**
 * More idiomatic use of the [Regex] class by short circuit the constructor.
 *
 * @param expression The regular expression.
 */
fun regexOf(@Language("RegExp") expression: String): Regex = Regex(expression)

/**
 * More idiomatic use of the [Regex] class by short circuit the constructor.
 *
 * @param expression The regular expression.
 * @param option An additional option for constructing the regex.
 * @param moreOptions An vararg for more options.
 */
fun regexOf(
        @Language("RegExp") expression: String,
        option: RegexOption,
        vararg moreOptions: RegexOption
): Regex = Regex(expression, setOf(option) + moreOptions)
