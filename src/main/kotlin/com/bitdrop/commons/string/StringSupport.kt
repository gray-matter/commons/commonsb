package com.bitdrop.commons.string

private const val SNAKE = '_'

fun String.toSnakeCase(upper: Boolean = false): String {

    if (isEmpty()) {
        return this
    }

    return StringBuilder().let { sb ->
        for ((i, c) in withIndex()) {
            val nc = if (upper) c.toUpperCase() else c.toLowerCase()
            if (c.isUpperCase()
                    && i > 0 // Do not make the insert an "_" if this is first character
                    && i + 1 < length
                    && (get(i + 1).isLowerCase()) // Do not want "_" if the next is lower case
            ) {
                sb.append(SNAKE)
            }
            sb.append(nc)
        }
        sb.toString()
    }
}

/**
 * Trim left and right side of each line and concatenate the lines.
 */
fun String.tr(margin: String? = null): String {

    val text = when (margin) {
        null -> trimIndent()
        else -> trimMargin(margin)
    }

    return text.lineSequence()
            .map(String::trimEnd)
            .filter(String::isNotEmpty)
            .joinToString(separator = "")

}
