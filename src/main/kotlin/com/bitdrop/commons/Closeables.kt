package com.bitdrop.commons

import java.io.IOException
import java.io.Closeable as IOClosable

interface SilentIOCloseable {
    fun closeSilent(): IOException?
}

interface SilentAutoCloseable {
    fun closeSilent(): Exception?
}

fun IOClosable.closeSilent(): IOException? {
    return when (this) {
        is SilentIOCloseable -> this.closeSilent()
        else -> try {
            close()
            null
        } catch (e: IOException) {
            e
        }
    }
}

fun AutoCloseable.closeSilent(): Exception? {
    return when (this) {
        is SilentAutoCloseable -> this.closeSilent()
        else -> try {
            close()
            null
        } catch (e: Exception) {
            e
        }
    }
}
