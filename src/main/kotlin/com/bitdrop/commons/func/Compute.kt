package com.bitdrop.commons.func

/**
 * A computation function which takes in [computation] to produce a result. Handy if one needs
 * to evaluate an computation within a constructor, as there is no explicit capturing of the `this`
 * reference (_unlike_ the [run] function in the standard library which will always attempt to capture
 * a reference to `this` within an constructor).
 *
 * @param T the type of result the action must produce.
 * @param computation The action to evaluate
 * @return An result of the computation.
 *
 * @see [run]
 */
inline fun <T> compute(computation: () -> T): T = computation()
