package com.bitdrop.commons.func

/**
 * This is classic monad, where the left value is the error,
 * and the right side the result.
 *
 * To create a [Either.Success] value, use the [success] function, and
 * and similarly, the [failure] function to create a [Either.Failure]
 *
 * This file also provides several operators which can be used
 * to chain, and transform results:
 *
 * 1. Use the [bindOn] function to bind a result, or error.
 * 2. Use the [andThen] function to continue with the next success full flow.
 * 3. Use the [andThenMap] function to convert input from one success value to another value.
 * 4. Use the [fold] function to convert either an [Error], or a [Success] to single value.
 * 5. Use the [swap] function to treat a result as error, and visa versa.
 *
 * **Working with the result**:
 *
 * Use the [getOrThrow] functions if you want to get the right hand side, and have the failure
 * be raised via an exception. If you do not supply a lambda to instantiate an exception,
 * this function will throw an [ExpectedResultOnEitherException].
 *
 * **Evaluating the error (Left side) of the monad:**
 *
 * The [error] property can be used to retrieve the left hand side, e.g. the error. A value of
 * `null` indicates that no error has been raised.
 *
 * If you want to get the result it exists, and also deal with the any error as side effect,
 * us the [getOr] function.
 *
 * @see onSuccess
 * @see onFailure
 * @see andThen
 * @see fold
 * @see getOrThrow
 *
 */
sealed class Either<out L, out R> {
    data class Success<R>(val result: R) : Either<Nothing, R>()
    data class Failure<L>(val error: L) : Either<L, Nothing>()
}

/**
 * Treat a success as failure, and visa versa a failure as success.
 */
fun <L, R> Either<L, R>.swap(): Either<R, L> {
    return when (this) {
        is Either.Success -> failure(result)
        is Either.Failure -> success(error)
    }
}

fun <L, R> success(result: R): Either<L, R> = Either.Success(result)
fun <L, R> failure(error: L): Either<L, R> = Either.Failure(error)

inline fun <L, R> Either<L, R>.andThen(action: (Either.Success<R>) -> Either<L, R>): Either<L, R> {
    return when (this) {
        is Either.Success -> action(this)
        is Either.Failure -> this
    }
}

inline fun <L, R, T> Either<L, R>.andThenMap(map: (Either.Success<R>) -> Either<L, T>): Either<L, T> {
    return when (this) {
        is Either.Success -> map(this)
        is Either.Failure -> this
    }
}

/**
 * Folds either a error [L] value, or result [R] into a single new value.
 */
inline fun <L, R, T> Either<L, R>.fold(ofError: (L) -> T, ofValue: (R) -> T): T {
    return when (this) {
        is Either.Success -> ofValue(result)
        is Either.Failure -> ofError(error)
    }
}

/**
 * Use this function to raise an error (if it occurred), before continuing.
 */
inline fun <L, R> Either<L, R>.getOrThrow(exceptionOf: (L) -> Exception): R {
    return when (this) {
        is Either.Success -> result
        is Either.Failure -> throw exceptionOf(error)
    }
}

class ExpectedResultOnEitherException internal constructor(
        @Suppress("MemberVisibilityCanBePrivate") val failure: Any
) : IllegalStateException("$failure") {
    init {
        if (failure is Throwable) {
            initCause(failure)
        }
    }
}

fun <L, R> Either<L, R>.getOrThrow(): R = getOrThrow { ExpectedResultOnEitherException(it as Any) }

fun <L, R> Either<L, R>.getOr(resultOfError: (L) -> R): R {
    return when (this) {
        is Either.Success -> result
        is Either.Failure -> resultOfError(error)
    }
}

val <L> Either<L, *>.error: L?
    get() = when (this) {
        is Either.Success -> null
        is Either.Failure -> error
    }

interface EitherBinding<L, R> {
    operator fun invoke(result: R)
    fun failed(e: L)
}

inline fun <L, R> bindOn(bind: (either: EitherBinding<L, R>) -> Unit): Either<L, R> {

    lateinit var either: Either<L, R>

    bind(object : EitherBinding<L, R> {

        override fun invoke(result: R) {
            either = success(result)
        }

        override fun failed(e: L) {
            either = failure(e)
        }

    })

    return either
}

inline fun <reified E, T> bind(action: () -> T): Either<E, T> where E : Exception {
    return bindOn { either: EitherBinding<E, T> ->
        try {
            either(action())
        } catch (e: Exception) {
            if (e is E) either.failed(e) else throw e
        }
    }
}
