package com.bitdrop.commons

import java.time.LocalDateTime
import java.time.ZoneId
import java.util.Date as LegacyDate

/**
 * Converts a local date to
 */
fun LocalDateTime.toLegacyDate(): LegacyDate {
    return LegacyDate.from(atZone(ZoneId.systemDefault()).toInstant())
}

fun LegacyDate.toLocalDateTime(): LocalDateTime {
    return toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime()
}
