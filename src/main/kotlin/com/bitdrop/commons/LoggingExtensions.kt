package com.bitdrop.commons

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.slf4j.helpers.NOPLogger

/**
 * A function returns a standard logger based on the class of [T]
 *
 * @param T The type the logger name should be derived from.
 * @return An instance of [Logger] which logs to the fully qualified value of the class name of [T]
 */
inline fun <reified T> loggerOf(): Logger = LoggerFactory.getLogger(T::class.java)


/**
 * Report the logging level
 */
enum class Level(val priority: Int) {
    Warn(3),
    Error(4),
    Trace(5),
    Debug(1),
    Info(2);
}


/**
 * Property to report on the level of this [Logger]. Not level may be `null` if no logging bridge is found on
 * the class path.
 */
val Logger.level: Level?
    get() {
        return when {
            isWarnEnabled -> Level.Warn
            isDebugEnabled -> Level.Debug
            isErrorEnabled -> Level.Error
            isTraceEnabled -> Level.Trace
            isInfoEnabled -> Level.Info
            else -> null
        }
    }


private val dummyLogger: Logger get() = LoggerFactory.getLogger("DummyLogger")

val loggingIsAvailable: Boolean get() = dummyLogger !is NOPLogger

inline fun Logger.debug(lazyMessage: () -> String) {
    if (isDebugEnabled) {
        debug(lazyMessage())
    }
}

inline fun Logger.debug(cause: Throwable, lazyMessage: () -> String) {
    if (isDebugEnabled) {
        debug(lazyMessage(), cause)
    }
}

inline fun Logger.warn(lazyMessage: () -> String) {
    if (isWarnEnabled) {
        warn(lazyMessage())
    }
}

inline fun Logger.warn(cause: Throwable, lazyMessage: () -> String) {
    if (isWarnEnabled) {
        warn(lazyMessage(), cause)
    }
}

inline fun Logger.info(lazyMessage: () -> String) {
    if (isInfoEnabled) {
        info(lazyMessage())
    }
}

inline fun Logger.info(cause: Throwable, lazyMessage: () -> String) {
    if (isInfoEnabled) {
        info(lazyMessage(), cause)
    }
}

fun interface LogMessage {
    fun log(message: String)
}

interface LogMessageWithCause : LogMessage {
    fun log(cause: Throwable, message: String)
}

inline fun Logger.infos(next: LogMessage.() -> Unit) {
    if (isInfoEnabled) {
        LogMessage { message -> info(message) }.next()
    }
}
inline fun Logger.debugs(crossinline debug: LogMessageWithCause.() -> Unit) {
    if (isDebugEnabled) {
        object : LogMessageWithCause {
            init {
                debug()
            }
            override fun log(cause: Throwable, message: String) {
                debug(message, cause)
            }

            override fun log(message: String) {
                debug(message)
            }
        }
    }
}