package com.bitdrop.commons.collections

/**
 * Drains a collection into another list.
 */
fun <T> MutableCollection<T>.drain(dest: MutableList<T> = arrayListOf(size)): MutableList<T> {
    dest.addAll(this)
    clear()
    return dest
}

/**
 * Drains each item individually.
 *
 * @param drainItem A code block which handles the draining of individual items.
 */
inline fun <T> MutableCollection<T>.drain(drainItem: (T) -> Unit) {
    drain(mutableListOf()).forEach(drainItem)
}

/**
 * Drains each _`key,value`_ pair individually
 */
inline fun <K, V> MutableMap<K, V>.drain(drainItem: (K, V) -> Unit) {
    entries.drain { (k, v) -> drainItem(k, v) }
}

/**
 * Produces an _`ArrayList`_ with an initial _`capacity`_.
 */
fun <T> arrayListOf(capacity:Int): java.util.ArrayList<T> = java.util.ArrayList(capacity)

private val defaultJoinKeyPair: (Any?, Any?) -> CharSequence = { k, v -> "$k=$v" }

fun <K, V> Map<K, V>.joinTo(
        dest: StringBuilder,
        separator: CharSequence = ", ",
        prefix: CharSequence = "",
        postfix: CharSequence = "",
        limit: Int = -1,
        truncated: CharSequence = "...",
        toString: (K, V) -> CharSequence = defaultJoinKeyPair
): StringBuilder {
    entries.joinTo(dest, separator, prefix, postfix, limit, truncated) { (k, v) -> toString(k, v) }
    return dest
}

fun <K, V> Map<K, V>.joinToString(
        separator: CharSequence = ", ",
        prefix: CharSequence = "",
        postfix: CharSequence = "",
        limit: Int = -1,
        truncated: CharSequence = "...",
        toString: (K, V) -> CharSequence = defaultJoinKeyPair): String = buildString {
    joinTo(this, separator, prefix, postfix, limit, truncated, toString)
}