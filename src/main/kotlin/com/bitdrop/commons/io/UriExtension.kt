package com.bitdrop.commons.io

import java.net.URI

/**
 * Creates a [URI] instance from a string value.
 *
 * @throws IllegalArgumentException if the uri string is not valid.
 */
fun uri(string: String): URI = URI.create(string)



