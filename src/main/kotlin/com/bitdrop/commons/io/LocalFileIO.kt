@file:Suppress("MemberVisibilityCanBePrivate", "CanBeParameter")

package com.bitdrop.commons.io

import java.io.File
import java.io.FileNotFoundException
import java.io.IOException
import java.io.RandomAccessFile
import java.nio.file.Path

/**
 * Exception to indicate that expected directory could not be found.
 */
class DirectoryNotFoundException(val path: File, message: String = "Directory not found: $path") : FileNotFoundException(message)

/**
 * Checks to see if `this` exists as directory. If it does it also checks if it refers
 * to a regular local file system directory.
 *
 * @param createIfNotExist Attempts to create an folder path if does not exists.
 *
 * @throws IOException If the unable to create a newly requested directory for any reason.
 * @throws DirectoryNotFoundException If the it [createIfNotExist] is set to false (default) and the directory does not exists.
 *
 * @return This file.
 */
fun File.directoryRequired(createIfNotExist: Boolean = false): File {

    if (!exists()) {
        if (createIfNotExist) {
            check(File::mkdirs) { "Unable to create required directory :$this" }
        } else throw DirectoryNotFoundException(this)
    }

    check(File::isDirectory) { "Expected file to be a regular directory here: $this" }

    return this
}

fun File.parentRequired(createIfNotExist: Boolean = false) {
    parentFile.directoryRequired(createIfNotExist)
}


/**
 * Checks to see if this file exists. If not throws an [FileNotFoundException] exception.
 *
 * @return This file.
 */
fun File.required(): File = apply {
    check(File::exists, ::FileNotFoundException) { "Required file here: $this" }
    check(File::isFile) { "Expected to find regular file here: $this" }
}

/**
 * This extension function checks forms the base for checking preconditions on local file objects. By default it
 * throws an [IOException] with the defined [lazyMessage].
 *
 * @param precondition The precondition which must pass.
 * @param newIOException A function which takes a message, and produces an [IOException] (or derived) instance.
 * @param lazyMessage A message will be passed to the [newIOException] function describing why the check failed.
 */
inline fun File.check(
        precondition: File.() -> Boolean,
        newIOException: (message: String) -> IOException = ::IOException,
        lazyMessage: () -> String
) {
    if (!precondition()) {
        throw newIOException(lazyMessage())
    }
}

/**
 * Wrapper for the [Path.of] function to reliably convert a path of strings to [File] instance.
 *
 * @param first The first part of the path.
 * @param moreParts The rest of the parts
 *
 * @return A `File` object with a correct path.
 *
 * @see Path.of
 */
fun file(first: String, vararg moreParts: String): File = moreParts.fold(File(first)) { parent, next -> File(parent, next) }
fun file(first: File, vararg moreParts: String): File = moreParts.fold(first) { parent, next -> File(parent, next) }

fun File.truncate(size: Long = 0L) {
    RandomAccessFile(this, "rws").use {
        it.setLength(size)
    }
}

fun File.isEmpty(): Boolean = when {
    !exists() -> throw FileNotFoundException("No file named: $this")
    isDirectory -> listFiles()?.isEmpty() ?: false
    else -> length() == 0L
}

fun File.isNotEmpty(): Boolean = !isEmpty()

fun File.contentAvailable(): Boolean = exists() && !isNotEmpty()

/**
 * Attempts to remove a file or directory from the current file system. If the case
 * of an regular file the file be deleted. In case of a directory the contents (including
 * the directory)  will be removed.
 *
 * __NOTE:__ A non existing file will be treated as an successful removal operation.
 *
 */
fun File.remove(): Boolean {
    return when {
        !exists() -> true
        isDirectory -> deleteRecursively()
        isFile -> delete()
        else -> throw IOException("Must either be a folder or regular file: $this")
    }
}

/**
 * Produces a child path, treating this file as the parent.
 */
fun File.child(path: String, vararg rest: String): File {
    return rest.fold(File(this, path)) { parent: File, child: String -> File(parent, child) }
}
