package com.bitdrop.commons.lang

/**
 * Try to use instance as specific type. This is a functional equivalent of the statement - ``any as? T``,
 *
 * @param obj The instance to we want to cast to the [targetClass]
 * @param targetClass The class we want.
 * @param T The type parameter for the target class.
 *
 * @return Either an instance of the target class, or `null` if cannot be cast.
 */
fun <T> tryCastOf(obj: Any?, targetClass: Class<T>): T? {
    return when {
        obj == null -> null
        targetClass.isInstance(obj) -> targetClass.cast(obj) as T
        else -> null
    }
}

inline fun <reified T> tryCastOf(obj: Any?): T? {
    return tryCastOf(obj, T::class.java)
}

/**
 * Attempts to perform an cast which disregards descendants of the [targetClass].
 *
 * @param obj to cast.
 * @param targetClass The expected target class of the object.
 * @return An instance of targetClass, or `null`
 */
fun <T> tryExactCastAs(obj: Any?, targetClass: Class<T>): T? {
    return when {
        obj == null -> null
        obj.javaClass == targetClass -> targetClass.cast(obj) as T
        else -> null
    }
}

inline fun <reified T> tryExactCastAs(obj: Any?): T? = tryExactCastAs(obj, T::class.java)
