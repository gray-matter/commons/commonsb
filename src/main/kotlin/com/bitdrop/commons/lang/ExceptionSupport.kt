@file:JvmName("ExceptionSupport")

package com.bitdrop.commons.lang

/**
 * Function which initiates the cause of an exception
 *
 * @return This exception
 */
fun <X> Throwable.causedBy(cause: X): X where X : Throwable {
    cause.initCause(this)
    return cause
}

/**
 * A convenience function to access the trace an exception down to the root cause.
 *
 * @receiver The throwable to access the causes with.
 * @return A sequence of exceptions (including `this` exception)
 */
fun Throwable.trace() = generateSequence(this, Throwable::cause)

/**
 * Trace this throwable exception to its root cause.
 */
fun Throwable.traceToRootCause(): Throwable = trace().last()

