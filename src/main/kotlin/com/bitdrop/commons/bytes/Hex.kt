@file:Suppress("PLATFORM_CLASS_MAPPED_TO_KOTLIN")

package com.bitdrop.commons.bytes

enum class HexStyle {
    LOWER,
    UPPER
}

/**
 * Converts a byte array to hexadecimal string.
 *
 * @param style Which digits to use, upper case or lower case.
 */

@JvmOverloads
fun ByteArray.encodeToHex(style: HexStyle = HexStyle.LOWER): String {

    if (isEmpty()) {
        return emptyString
    }

    val digits = when (style) {
        HexStyle.LOWER -> lowerHexDigits
        HexStyle.UPPER -> upperHexDigits
    }

    return buildString(2 * size) {
        for (byte in this@encodeToHex) {
            val b = byte.toInt() and 0xff
            append(digits[b shr 4])
            append(digits[b and 0x0f])
        }
    }
}

@Suppress("SpellCheckingInspection")
private const val lowerHexDigits = "0123456789abcdef"
@Suppress("SpellCheckingInspection")
private const val upperHexDigits = "0123456789ABCDEF"
private const val emptyString = ""
