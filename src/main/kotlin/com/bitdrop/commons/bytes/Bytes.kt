package com.bitdrop.commons.bytes

import java.nio.ByteBuffer

fun Int.toByteArray(): ByteArray {
    val bytes = ByteArray(Int.SIZE_BYTES)
    ByteBuffer.wrap(bytes).putInt(this)
    return bytes
}

interface BytesPresentable {
    fun toByteArray(): ByteArray
}


fun Any?.toByteArray():ByteArray? {
    return when (this) {
        null -> null
        is BytesPresentable -> toByteArray()
        is Int -> toByteArray()
        else -> null
    }
}

