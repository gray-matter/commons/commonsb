package com.bitdrop.commons.bytes

import java.security.MessageDigest

/**
 * Convenience function to hash any array of bytes.
 *
 * @param algorithm Any supported message digest _`algorithm`_.
 */
fun ByteArray.hash(algorithm: String): ByteArray {
    return MessageDigest.getInstance(algorithm).digest(this)
}

/**
 * Convenience function to hash any array of bytes.
 *
 * @param provider Any message digest provider other than the default/builtin supplied ones.
 * @param algorithm Any supported message digest _`algorithm`_ for a given message digest _`provider`_.
 */
fun ByteArray.hash(algorithm: String, provider: String): ByteArray {
    return MessageDigest.getInstance(algorithm, provider).digest(this)
}

private val accumulateHash = { hash: Int, obj: Any? ->
    31 * (if (hash == 0) 1 else hash) + (obj?.hashCode() ?: 0)
}

/**
 * Computes a hash from a collection of values.
 */
fun Collection<Any?>?.computeHash(): Int = this?.takeUnless { it.isEmpty() }?.fold(0, accumulateHash) ?: 0

/**
 * Computes a hash from any sequence of values.
 */
fun Sequence<Any?>?.computeHash(): Int = this?.fold(0, accumulateHash) ?: 0

/**
 * Computes a hash from an array of values.
 */
fun Array<*>.computeHash(): Int = takeUnless { it.isEmpty() }?.fold(0, accumulateHash) ?: 0
