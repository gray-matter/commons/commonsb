@file:JvmName("Truth")
package com.bitdrop.commons

/**
 * A function to translate a `true`/`false` value to two possible values.
 *
 * Use this function to convert a boolean to integers, for example: `computes().truth(1,0)`
 */
fun <T> Boolean.truth(truth: T, notTrue: T): T = if (this) truth else notTrue

/**
 * Extensions property to see if a atomic reference is null.
 */
val <T> T?.isNull: Boolean get() = this == null

/**
 * Extension property to see if a atomic reference is not null.
 */
val <T> T?.isNotNull: Boolean get() = this != null

/**
 * Handy short cut for _`Boolean.truth("yes","no")`_
 */
val Boolean.yesOrNo: String get() = truth("yes", "no")
