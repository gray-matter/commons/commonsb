package com.bitdrop.commons.concurrency

import java.util.concurrent.atomic.*


/**
 * Produces an atomic reference to an _`int`_.
 */
fun atomicOf(int: Int): AtomicInteger = AtomicInteger(int)

/**
 * Produces an atomic reference to a `_long`_.
 */
fun atomicOf(long: Long): AtomicLong = AtomicLong(long)

/**
 * Produces an atomic reference to a `_boolean`_ value.
 */
fun atomicOf(boolean: Boolean): AtomicBoolean = AtomicBoolean(boolean)

/**
 * Produces an atomic reference to an array of _ints_.
 */
fun atomicOf(intArray: IntArray): AtomicIntegerArray = AtomicIntegerArray(intArray)

/**
 * Produces an atomic reference to an array of _longs_.
 */
fun atomicOf(longArray: LongArray): AtomicLongArray = AtomicLongArray(longArray)

/**
 * Produces an atomic reference to any object instance of type `T`
 */
fun <T> atomicOf(value: T): AtomicReference<T> = AtomicReference(value)

/**
 * Produces an atomic reference to a nullable instance of type `T`
 */
fun <T> atomicOf(): AtomicReference<T?> = AtomicReference(null)

/**
 * Determine if a value has been set. __Note:__ that the underlying value referenced can still be _`null`_.
 */
inline val <V> AtomicReference<V>.isSet: Boolean get() = get() != null

/**
 * Determine if a value has not been set. __Note:__ that the underlying value referenced can still be _`null`_.
 */
inline val <V> AtomicReference<V>.isGone: Boolean get() = get() == null

/**
 * Determine if atomic boolean is _`true`_.
 */
inline val AtomicBoolean.flagged: Boolean get() = get()


fun AtomicReference<*>.unset() {
    set(null)
}

var <T> AtomicReference<T>.value: T
    get() {
        val v: T? = get() ?: error("Value has been unset, or not been set.")
        return v!!
    }
    set(value) {
        this.set(value)
    }

inline fun AtomicBoolean.once(performOnce: () -> Unit) {
    if (get()) return
    performOnce()
    set(true)
}

