package com.bitdrop.commons.concurrency

import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentMap
import java.util.concurrent.ConcurrentNavigableMap
import java.util.concurrent.ConcurrentSkipListMap

fun <K, V> concurrentMapOf(vararg keyValues: Pair<K, V>): ConcurrentMap<K, V> {
    return ConcurrentHashMap<K, V>().apply {
        putAll(keyValues)
    }
}

fun <K, V> concurrentMapOf(keyOrder: Comparator<K>, vararg keyValues: Pair<K, V>): ConcurrentNavigableMap<K, V> {
    return ConcurrentSkipListMap<K, V>(keyOrder).apply {
        putAll(keyValues)
    }
}

