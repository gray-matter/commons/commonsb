package com.bitdrop.commons.io

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class LocalFileIOKtTest {

    @Test
    fun child() {
        val root = file("root")
        val actual = root.child("data", "1", "2")
        val expected = file("root/data/1/2")
        assertEquals(expected, actual)
    }
}
