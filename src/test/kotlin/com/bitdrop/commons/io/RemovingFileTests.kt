package com.bitdrop.commons.io

import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import java.io.File

internal class RemovingFileTests {

    private lateinit var dir: File

    @BeforeEach
    fun setUp(@TempDir givenTempDir: File) {
        dir = file(givenTempDir, "removingFileTests").directoryRequired(createIfNotExist = true)
    }

    @Test
    fun removingDirectoryTree() {

        // Given a tree of directories and files under tempDir
        file(dir, "src/main/kotlin").directoryRequired(true)
        file(dir, "src/main/resources").directoryRequired(true)

        // When removing the directory
        dir.remove()

        // Then there dir should not exists
        assertFalse(dir.exists())
    }

    @Test
    fun removingFile() {

        // Given a regular file with content.
        val regularFile = file(dir, "data").requiredRandomBinaryFile()

        // When removing the file
        regularFile.remove()

        // Then the file should not exists.
        assertFalse(regularFile.exists())

        // Then the parent folder should still exists.
        assertTrue(regularFile.parentFile.exists())
    }

}
