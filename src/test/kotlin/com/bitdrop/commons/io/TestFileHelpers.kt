package com.bitdrop.commons.io

import org.junit.jupiter.api.Assertions
import java.io.File
import kotlin.random.Random

fun File.requiredRandomBinaryFile(minBytes: Int = 0, maxBytes: Int = 1024): File {

    require(minBytes >= 0) { "Min file size($minBytes) must zero or more." }
    require(maxBytes >= minBytes) { "Max file size($maxBytes) should be equal, or more than $minBytes" }

    Assertions.assertTrue(parentFile.exists(), "Parent directory for file does not exists: $this")

    val fileSize = when (minBytes) {
        maxBytes -> maxBytes
        else -> Random.nextInt(minBytes, maxBytes)
    }

    val data = Random.nextBytes(fileSize)
    writeBytes(data)

    println("Created random file with $fileSize bytes: $this")

    return this
}
