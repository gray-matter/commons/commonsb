package com.bitdrop.commons.io

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import java.io.File
import java.util.*


@DisplayName("Tests pertaining to common file handling tasks.")
internal class TruncateFileTests {

    private lateinit var fixture: Fixture

    @BeforeEach
    fun setUp(@TempDir tempDir: File) {
        fixture = Fixture(tempDir)
    }

    @Test
    fun truncatingFileToZeroShouldResultInEmptyFile() {
        fixture.givenNonEmptyRegularFile()
        fixture.givenTruncatingSizeToZero()
        fixture.whenTruncating()
        fixture.thenRegularFileLengthShouldReflectTheExpectedSize()
    }

    @Test
    fun truncatingFileToSpecificSize() {
        fixture.givenNonEmptyRegularFile()
        fixture.givenExpectingTruncatingToLessThanOriginalSize()
        fixture.whenTruncating()
        fixture.thenRegularFileLengthShouldReflectTheExpectedSize()
    }


    private class Fixture(private val tempDir: File) {

        private var expectedTruncatedSize: Long = -1L
        private lateinit var regularNonEmptyFile: File

        fun givenNonEmptyRegularFile() {
            val regularFileName = "regularNonEmptyFile_${UUID.randomUUID().toString().replace("-", "")}"
            regularNonEmptyFile = file(tempDir, regularFileName).requiredRandomBinaryFile()
        }


        fun thenRegularFileLengthShouldReflectTheExpectedSize() {
            assertEquals(expectedTruncatedSize, regularNonEmptyFile.length())
        }

        fun givenExpectingTruncatingToLessThanOriginalSize() {
            when {
                expectedTruncatedSize < 5L -> expectedTruncatedSize = 1L
                else -> expectedTruncatedSize -= 5L
            }
        }

        fun givenTruncatingSizeToZero() {
            expectedTruncatedSize = 0L
        }

        fun whenTruncating() {
            regularNonEmptyFile.truncate(expectedTruncatedSize)
        }

    }
}


