package com.bitdrop.commons

import com.bitdrop.commons.string.tr
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class StdLibraryExtensionsKtTest {

    @Test
    fun testTrimToLine() {

        val expected = "dbc:sqlserver://kmimkmproduction.database.windows.net:1433;database=mkmproduction_Dev;user=mkm_Admin@kmimkmproduction;password={your_password_here};encrypt=true;trustServerCertificate=false;hostNameInCertificate=*.database.windows.net;loginTimeout=30;"

        val actual = """
            dbc:sqlserver://kmimkmproduction.database.windows.net:1433;
            database=mkmproduction_Dev;
            user=mkm_Admin@kmimkmproduction;
            password={your_password_here};
            encrypt=true;
            trustServerCertificate=false;
            hostNameInCertificate=*.database.windows.net;
            loginTimeout=30;
            """.tr()

        assertEquals(expected, actual)
    }

    @Test
    fun testTrimToLineWithSpaces() {
        val expected = "Jumbo is ok"
        val actual = """
           |Jumbo
           | is
           | ok
        """.tr("|")
        assertEquals(expected, actual)
    }


}
