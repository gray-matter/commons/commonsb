package com.bitdrop.commons

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import java.time.LocalDateTime
import java.time.Month
import java.util.*

internal class LegacyDateSupportKtTest {

    @Test
    fun toLegacyDate() {
        val expected = Calendar.getInstance().run {
            set(Calendar.YEAR, 2020)
            set(Calendar.MONTH, Calendar.FEBRUARY)
            set(Calendar.DATE, 1)
            set(Calendar.HOUR_OF_DAY, 16)
            set(Calendar.MINUTE, 23)
            set(Calendar.SECOND, 0)
            set(Calendar.MILLISECOND, 0)
            time
        }

        val d = LocalDateTime.of(2020, Month.FEBRUARY, 1, 16, 23, 0)

        val actual = d.toLegacyDate()

        println(actual)
        println(expected)

        assertEquals(expected, actual)
    }

    @Test
    fun toLocalDateTime() {

        val expected = LocalDateTime.of(1971, Month.APRIL, 29, 10, 0, 0, 0)

        val actual = Calendar.getInstance().run {
            set(Calendar.YEAR, 1971)
            set(Calendar.MONTH, Calendar.APRIL)
            set(Calendar.DATE, 29)
            set(Calendar.HOUR_OF_DAY, 10)
            set(Calendar.MINUTE, 0)
            set(Calendar.SECOND, 0)
            set(Calendar.MILLISECOND, 0)
            time.toLocalDateTime()
        }

        assertEquals(expected, actual)

    }
}
