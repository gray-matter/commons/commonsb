package com.bitdrop.commons

import com.bitdrop.commons.collections.drain
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import java.io.Closeable
import kotlin.random.Random

internal class CollectionExtensionsSamples {

    @Test
    fun drainCollection() {

        // Given
        val collection = arrayListOf(1, 2, 3, 'a', "b", 'c')
        val countedItems = collection.size

        // When
        val drained = collection.drain()

        // Then
        assertTrue(collection.isEmpty(), "All elements should have been drained.")
        assertEquals(countedItems, drained.size, "Drained items should the same amount of items originally placed it it.")

    }

    @Test
    fun drainItemsFromCollectionWithFunction() {

        // Given
        val numberOfClosable = 6
        var actuallyClosed = 0
        val collection = arrayListOf<Closeable>().apply {
            repeat(numberOfClosable) {
                add(mockk { every { close() } answers { ++actuallyClosed } })
            }
        }

        // When
        collection.drain(Closeable::close)

        // Then
        assertTrue(collection.isEmpty())
        assertEquals(numberOfClosable, actuallyClosed)
    }

    @Test
    fun drainKeyValuesFromMap() {

        // Given
        var actuallyDrained = 0
        val expectedToDrain = Random.nextInt(1, 10)
        val mapWithCloseables = mutableMapOf<Int, Closeable>().apply {
            (1..expectedToDrain).forEach { key ->
                put(key, mockk { every { close() } answers { ++actuallyDrained } })
            }
        }

        // When
        mapWithCloseables.drain { _, closeable -> closeable.close() }

        // Then
        assertTrue(mapWithCloseables.isEmpty(), "Map should have been drained.")
        assertEquals(expectedToDrain, actuallyDrained)

    }

}