package com.bitdrop.commons.string

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource

internal class StringSupportKtTest {

    @ParameterizedTest(name = "given:{0} ✔: {1}")
    @CsvSource(value = [
        "''              |''",
        "SnakeCase       |snake_case",
        "AB1             |ab1",
        "CreativeProject |creative_project"], delimiterString = "|")
    fun testToSnake(given: String, expected: String) {
        val actual = given.toSnakeCase()
        println("GIVEN: $given")
        println("ACTUAL: $actual")
        assertEquals(expected, actual)
    }
}
