package com.bitdrop.commons

import io.mockk.*
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.slf4j.Logger

internal class LoggingExtensionsKtTest {

    private lateinit var log: Logger

    @Test
    fun testLogMany() {
        val actual = mutableListOf<String>()
        log = mockk("mockedLogger")
        every { log.isInfoEnabled } returns true
        every { log.info(capture(actual)) } just runs
        val expected = listOf("info 1", "info 2", "info 3")
        log.infos {
            log("info 1")
            log("info 2")
            log("info 3")
        }
        assertEquals(expected, actual)
    }

    @Test
    fun testDebugMany() {

        val debugStatements = mutableListOf<Pair<String, Throwable?>>()

        log = mockk("mockLog") {
            every { isDebugEnabled } returns true
            every { debug(any()) } answers { debugStatements += arg<String>(0) to null }
            every { debug(any<String>(),any()) } answers {
                val message: String = arg(0)
                val cause: Throwable = arg(1)
                debugStatements += message to cause
            }
        }

        log.debugs {
            log("debug 1")
            log(RuntimeException(), "debug 2")
        }

        fun Pair<String, Throwable?>.assertThat(expectedMessage: String, expectedThrowable: (actual: Throwable?) -> Boolean) {
            val (actualMessage, actualThrowable) = this
            assertEquals(expectedMessage, actualMessage, "expected debug message.")
            assertTrue(expectedThrowable(actualThrowable))
        }

        assertEquals(2, debugStatements.size, "Expected 2 debug statements.")
        debugStatements[0].assertThat("debug 1") { throwable -> throwable == null}
        debugStatements[1].assertThat("debug 2") { throwable -> throwable is RuntimeException }
    }
}