package com.bitdrop.commons

import io.mockk.*
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import java.io.Closeable
import java.io.EOFException
import java.io.IOException

@DisplayName("Tests to verify the correct (and intended) use of the closeables.")
internal class CloseablesKtTest {

    private lateinit var fixture: Fixture

    @BeforeEach
    fun setUp() {
        fixture = Fixture()
    }

    @Test
    @DisplayName("Silent closing a stream should allow for the collecting of the thrown exception.")
    fun silentClosingShouldAllowCollectionOfTheThrownException() {
        fixture.givenCloseableWhichThrowsIOException()
        fixture.whenClosingSilent()
        fixture.thenEOFShouldHaveBeenCaptured()
    }

    @Test
    @DisplayName("Silent closing a stream should delegate to the implementation if supported.")
    fun silentClosingShouldDelegateToApiCloseSilentContract() {
        fixture.givenCloseableWhichImplementsSilentClosingInterface()
        fixture.whenClosingSilent()
        fixture.thenImplementationShouldHaveHandledTheClosing()
    }

    private class Fixture {

        private var silentIOException: IOException? = null
        private lateinit var subject: Closeable

        fun givenCloseableWhichThrowsIOException() {
            subject = mockk { every { close() } answers { throw EOFException() } }
        }

        fun whenClosingSilent() {
            silentIOException = subject.closeSilent()
        }

        fun thenEOFShouldHaveBeenCaptured() {
            assertTrue(silentIOException is EOFException)
        }

        fun givenCloseableWhichImplementsSilentClosingInterface() {
            subject = mockk(moreInterfaces = *arrayOf(SilentIOCloseable::class))
            every { subject.close() } just Runs
            every { (subject as SilentIOCloseable).closeSilent() } answers { IOException() }
        }

        fun thenImplementationShouldHaveHandledTheClosing() {

            verify(inverse = true) { subject.close() }
            verify(exactly = 1) { (subject as SilentIOCloseable).closeSilent() }

            assertNotNull(silentIOException)
        }
    }

}
