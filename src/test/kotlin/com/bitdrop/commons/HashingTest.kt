package com.bitdrop.commons

import org.junit.jupiter.api.Test
import java.security.Provider
import java.security.Security

internal class HashingTest {

    @Test
    fun printSupportedHashes() {


        Security.getProviders().forEach { provider ->
            println(provider.name)
            println("-------------------------------------------------------------------------")
            provider.services.forEach { service: Provider.Service? ->
                if (service != null && service.type == "MessageDigest") {
                    println(service.algorithm)
                }
            }
            println()
        }
    }


}