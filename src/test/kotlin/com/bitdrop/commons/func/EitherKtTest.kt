package com.bitdrop.commons.func

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

internal class EitherKtTest {

    @Test
    fun testGetOrThrowWithException() {
        val r = bindOn<String, Int> { bind -> bind(10) }.andThen { failure("error_1") }
        val ex = assertThrows<Exception> { println(r.getOrThrow { Exception("Boo!") }) }
        assertEquals("Boo!", ex.message)
    }

    @Test
    fun testGetOrThrow() {
        val r = bindOn<String, Int> { bind -> bind(10) }.andThen { failure("error_1") }
        val ex = assertThrows<IllegalStateException> { println(r.getOrThrow()) }
        println(ex.message)
    }

}
