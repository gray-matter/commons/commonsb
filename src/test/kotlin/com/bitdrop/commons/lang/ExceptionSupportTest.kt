package com.bitdrop.commons.lang

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import java.io.IOException
import java.net.SocketException

internal class ExceptionSupportTest {

    private lateinit var givenThrowable: Throwable

    @Test
    fun traceShouldIncludeLastExceptionThrown() {
        val trace = givenThrowable.trace().toSet()
        assertTrue(givenThrowable in trace)
    }

    @Test
    fun causedByDslWorkAsIntended() {
        givenThrowable = Exception("1").causedBy(Exception("2")).causedBy(Exception("3"))
        val actual = givenThrowable.trace().map { it.message }.toList()
        val expected = listOf("3", "2", "1")
        assertEquals(expected, actual)
    }

    @Test
    fun traceToRootCause() {
        val rootCause = SocketException("socketIOException")
        val generalException = IOException(rootCause)
        givenThrowable = Exception("businessException",generalException)
        val actual = givenThrowable.traceToRootCause().message
        val expected = rootCause.message
        assertEquals(expected, actual)
    }

}
