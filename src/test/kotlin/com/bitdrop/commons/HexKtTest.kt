package com.bitdrop.commons

import com.bitdrop.commons.bytes.HexStyle
import com.bitdrop.commons.bytes.encodeToHex
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import java.io.ByteArrayOutputStream

internal class HexKtTest {

    @ParameterizedTest
    @MethodSource()
    fun testEncodingOfByteArrayToHex(actualBytes: ByteArray, style: HexStyle, expectedHexString: String) {
        val actualHexString = actualBytes.encodeToHex(style)
        assertEquals(expectedHexString, actualHexString)
    }

    fun testEncodingOfByteArrayToHex() = listOf<Array<out Any>>(
            a(b(0xba, 0x12), HexStyle.LOWER, "ba12"),
            a(b(), HexStyle.LOWER, ""),
            a(b(0x6c, 0x01, 0x00, 0x12), HexStyle.UPPER, "6C010012"))
            .stream()


    companion object {

        private fun b(vararg b: Int): ByteArray {
            return ByteArrayOutputStream().run {
                b.forEach(this::write)
                toByteArray()
            }
        }

        private fun <T> a(vararg all: T): Array<out T> = all
    }

}