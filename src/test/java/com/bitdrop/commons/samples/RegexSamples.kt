package com.bitdrop.commons.samples

import com.bitdrop.commons.string.regexOf
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll
import org.junit.jupiter.api.fail

internal class RegexSamples {

    @Test
    fun testIdiomaticUsage() {
        val re = regexOf("(\\d{4})-(\\d{2})-(\\d{2})")
        val dateString = "2020-04-29"
        val (year, month, day) = re.find(dateString)?.destructured ?: fail("No match for $dateString")
        assertAll({ year == "2020" }, { month == "04" }, { day == "29" })
    }

}
