package com.bitdrop.commons.collections;


import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.bitdrop.commons.LoggingExtensionsKt.getLevel;
import static com.bitdrop.commons.LoggingExtensionsKt.getLoggingIsAvailable;
import static com.bitdrop.commons.Truth.getYesOrNo;

class LoggingSample {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoggingSample.class);

    @Test
    void testLevels() {
        System.out.println(getLevel(LOGGER));
    }

    @Test
    void testConfig() {
        System.out.println("Logger is available: "+ getYesOrNo(getLoggingIsAvailable()) );
    }

}
