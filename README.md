# Commons Library

> A collection of convenience Kotlin functions and classes I like to use in some projects, but hate to keep on re-witting. 😢



## Overview

This library is build in the spirit of *_Pimp your Library_*, but in this case the library here refers to the JDK, as well as the underlying Kotlin standard library. Some of these are just one-liners, like replacing the constructor with a function. These one-liners have become, for better or worse, a convention in Kotlin. So I've decided to stop typing these one-liners and just collect in them in one library. These include:

\- Class casting functions which will return `null` if object is not of a specific type, or if it is direct descendant.

\- Idiomatic `regex()` function to produce Kotlin regular expression instances.

\- Function to check bounds when accessing an array with a detail explanation of which parameters are invalid.

\- Producing internal hash code for objects.

The JDK itself is filled with examples where there are just low level APIs, but not high level functions. This leads to error prone implementations because of the needlessly requirement of remembering the correct value of the arguments. Case in point consider the use if the Java `MessageDigest` class, which requires the user to remember the exact name of the message digest algorithm and provider. Failing to provide the *_exact_* names *_will_* result in a nasty runtime exception. Would it not have been better to have enum or fixed set of constants, rather? Typos happen so easy. Worse: strings are untyped, so the compiler will never ever catch the insertion of accidental keystroke in a parameter value.

In addition to higher level API, the library also expose idiomatic access for concurrent hash maps, and atomic locks. This includes property based access to atomic values.

Last some file handling functions and handling of closing of resources.

Specifically the use of the `java.io.File` class. Regardless of the many problems with this class (such as not really making a distinction between different kind of file objects existing on file system), is still a nice class to use for 99% of the time. With this is mind this library also add some convenience extensions functions to the `java.io.File` instance.

The liberal use of `IOExceptions` are most the time a good thing, except when it comes the closing down an I/O stream. Here the caller would need some more control, especially when closing more than one input/output streams. With this in mind this library provide a utility function which close a stream, and report any thrown `IOException`, or `null` if none.

## Navigating the library

Library is divided into the following packages: 

\* `com.bitdrop.commons` -- Extensions to the standard Kotlin library.

\* `com.bitdrop.commons.bytes` -- Bytes, hashing etc.

\* `com.bitdrop.commons.collections` -- Extensions and specialized collections.

\* `com.bitdrop.commons.concurrency` -- Concurrent collections and other extensions.

\* `com.bitdrop.commons.io` -- File I/O and stream handling.

\* `com.bitdrop.commons.func` -- Functional goodies

### Conventions

In general some functions also conform to a specific conventions:

#### Operations which could fail

These are functions which the caller expects to may fail. They should start with the prefix `try...`, and returns a `null` in case of a failure, instead of the expected result: for example:

```kotlin
val w: Widget? = tryExactCastAs<Widget>(arg)
```

#### Operations which should not fail

These are operations which are *_not expected_* to fail. In the case of failure these function will fail with an appropriate exception.

```kotlin
val reportFile = File(location, name).parentRequired(true)
```

This above function call will ensure that the parent directory exists, and if not will attempt to create it. Failing this, an appropriate `IOException` will be thrown.

#### Operations which must fail silent

These are functions which may fail, but it fails should do silently by reporting the failure. They should always end in the `Silent` suffix.

As an example consider:

```kotlin
val ex: IOException = fileInput.closeSilent() ?: return
// Not returned, lets handle the IOException:
log.warn("Failed to close file input properly.", ex)
```





