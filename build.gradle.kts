import com.jfrog.bintray.gradle.BintrayExtension.PackageConfig
import com.jfrog.bintray.gradle.BintrayExtension.VersionConfig
import com.jfrog.bintray.gradle.tasks.BintrayUploadTask
import org.gradle.api.plugins.JavaBasePlugin.DOCUMENTATION_GROUP
import org.gradle.jvm.tasks.Jar
import org.gradle.tooling.BuildException
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

/**
 * Basic module information about this module.
 */

val vcsConnectorGit = "git@gitlab.com:moon-bean/commons/commonsb.git"
val vssConnectorWeb = "https://gitlab.com/moon-bean/commons/commonsb"
val moduleNameSpace = "com.bitdrop"
val moduleVersion = "5.2.0-SNAPSHOT"
val moduleJvmTarget = "1.8"
val moduleInception = "2020"
val moduleDescription = """|
    |A common set of functions and classes I like to use across all my projects. Includes
    |some refinements such as class casting and hashing functions.
    |""".trimMargin()

val moduleLicense =License(
        id = "APACHE-2.0",
        name = "The Apache License, Version 2.0",
        url = "https://opensource.org/licenses/Apache-2.0")

data class License(
        val name: String,
        val url: String,
        val id: String
)

group = moduleNameSpace
version = moduleVersion

plugins {
    // Apply the Kotlin JVM plugin to add support for Kotlin.
    id("org.jetbrains.kotlin.jvm") version "1.4.0"
    id("com.jfrog.bintray") version "1.8.5"
    id("org.jetbrains.dokka") version "1.4.0"
    `java-library`
    `maven-publish`
}

repositories {
    mavenCentral()
    jcenter()
}

dependencies {
    // Align versions of all Kotlin components
    implementation(platform("org.jetbrains.kotlin:kotlin-bom"))
    implementation(platform("org.springframework.boot:spring-boot-dependencies:2.3.1.RELEASE"))
    implementation(kotlin("stdlib"))
    implementation(kotlin("reflect"))
    implementation("org.slf4j:slf4j-api")
    testImplementation("org.junit.jupiter:junit-jupiter-api")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
    testRuntimeOnly("org.slf4j:slf4j-simple")
    testImplementation("org.junit.jupiter:junit-jupiter-params")
    testImplementation("io.mockk:mockk:1.10.0")
}

tasks.withType<Test> {
    systemProperty("junit.jupiter.extensions.autodetection.enabled", "true")
    systemProperty("junit.jupiter.testinstance.lifecycle.default", "per_class")
    useJUnitPlatform {
        includeEngines.add("junit-jupiter")
        excludeEngines.add("junit-vintage")
    }
    testLogging {
        events("failed", "passed", "skipped")
    }
}

tasks.dokkaHtml.configure {
    outputDirectory.set(buildDir.resolve("dokka"))
}

tasks.withType<JavaCompile> {
    sourceCompatibility = moduleJvmTarget
    targetCompatibility = moduleJvmTarget
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        jvmTarget = moduleJvmTarget
    }
}

val sourceJar by tasks.creating(Jar::class) {
    group = DOCUMENTATION_GROUP
    description = "Package library sources"
    archiveClassifier.set("sources")
    from("src/main/kotlin")
}

val publicationName = project.name

publishing {
    publications {
        create<MavenPublication>(publicationName) {
            from(components["java"])
            artifact(sourceJar)
            groupId = moduleNameSpace
            version = moduleVersion
            pom {
                description.set(moduleDescription)
                inceptionYear.set(moduleInception)
                licenses {
                    license {
                        url.set(moduleLicense.url)
                        name.set(moduleLicense.name)
                        distribution.set("commons")
                    }
                }
                developers {
                    developer {
                        name.set("andries")
                        email.set("andries.fc@gmail.com")
                        roles.set(listOf("author"))
                        organization {
                            name.set("BitDrop")
                            url.set(vssConnectorWeb)
                        }
                    }
                }
                scm {
                    connection.set(vcsConnectorGit)
                    url.set(vssConnectorWeb)
                    developerConnection.set("scm:$vcsConnectorGit")
                }
            }
        }
    }
}

tasks.withType<BintrayUploadTask> {
    val user = findProperty("bintrayUser") as String?
    val key = findProperty("bintrayApiKey") as String?
    val enabled = !(key == null || user == null)
    doFirst {
        if (!enabled) {
            var messages = arrayOf("Unable to perform ${BintrayUploadTask::class.simpleName}:")
            if (user == null) messages += "No `bintrayUser` found."
            if (key == null) messages += "No `bintrayApiKey` found."
            throw BuildException(messages.joinToString(" "), null)
        }
    }
}

bintray {
    user = findProperty("bintrayUser") as String?
    key = findProperty("bintrayApiKey") as String?
    pkg(delegateClosureOf<PackageConfig> {
        repo = "maven"
        name = publicationName
        setLicenses(moduleLicense.id)
        setPublications(publicationName)
        userOrg = findProperty("bintrayUserOrg") as String?
        desc = moduleDescription.replace(Regex("\\s{2,}"), " ")
        publish = false
        vcsUrl = vssConnectorWeb
        setLabels("kotlin", "utilities", "commons")
        version(delegateClosureOf<VersionConfig> {
            name = moduleVersion
            desc = moduleDescription
            vcsTag = "v$moduleVersion"
        })
    })
}
