
rootProject.name = "bitdrop-commons"

pluginManagement {
    repositories {
        gradlePluginPortal()
        jcenter()
    }
}